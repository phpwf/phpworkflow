CREATE TABLE `workflow` (
  `workflow_id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  `type`	TEXT NOT NULL,
  `context`	TEXT NOT NULL,
  `create_dt`	TEXT NOT NULL,
  `start_dt`	NUMERIC NOT NULL,
  `finish_dt`	TEXT NOT NULL,
  `lock`	TEXT NOT NULL DEFAULT '',
  `status`	TEXT NOT NULL,
  `error_count`	INTEGER NOT NULL DEFAULT 0
);

CREATE TABLE `event` (
  `event_id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  `type`	TEXT NOT NULL,
  `context`	TEXT NOT NULL,
  `create_dt`	TEXT NOT NULL,
  `process_dt`	TEXT NOT NULL,
  `status`	TEXT NOT NULL,
  `workflow_id`	INTEGER NOT NULL
);

CREATE TABLE `subscription` (
  `status`	INTEGER NOT NULL,
  `event_type`	TEXT NOT NULL,
  `context_key`	TEXT NOT NULL,
  `context_value`	TEXT NOT NULL,
  `workflow_id`	INTEGER NOT NULL,
  `update_dt`	TEXT NOT NULL,
  PRIMARY KEY(status,event_type,context_key,context_value,workflow_id)
);

CREATE TABLE `log` (
  `log_id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  `workflow_id`	INTEGER NOT NULL,
  `create_dt`	TEXT NOT NULL,
  `log_text`	TEXT NOT NULL
);

CREATE INDEX `log_wf_ind` ON `log` (`workflow_id` ,`create_dt` );
CREATE INDEX `workflow_queue_ind` ON `workflow` (`status` ,`finish_dt` );
CREATE INDEX `event_queue_ind` ON `event` (`status` ,`process_dt` );
CREATE INDEX `subscription_ind` ON `subscription` (`status` ,`update_dt` );