<?php
/**
 * Created by PhpStorm.
 * User: Rus
 * Description: PhpWorkflow TODO: add description
 */

namespace Workflow;


interface IFactory {
    public function new_workflow($type);
    public function new_event($type);
}