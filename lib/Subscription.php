<?php
/**
 * Created by PhpStorm.
 * User: Rus
 * Description: PhpWorkflow TODO: add description
 */

namespace Workflow;

class Subscription {
    public $event_type;
    public $context_key;
    public $context_value;

    public function __construct($event_type, $context_key='', $context_value='') {
        $this->event_type=$event_type;
        $this->context_key=$context_key;
        $this->context_value=$context_value;
    }
}