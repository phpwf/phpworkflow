<?php
/**
 * Created by PhpStorm.
 * User: Rus
 * Description: PhpWorkflow TODO: add description
 */

namespace Workflow;

interface ILogger {
    // Logging channels
    const LOG_OFF = 0;
    const LOG_STDOUT = 1;
    const LOG_CONSOLE = 2;
    const LOG_FILE = 3;
    const LOG_DATABASE = 4;
    const LOG_LOGGER = 5;

    // Log levels
    const DEBUG=0;
    const INFO=1;
    const WARN=2;
    const ERROR=3;

    public function error($message);
    public function warn($message);
    public function info($message);
    public function debug($message);
}