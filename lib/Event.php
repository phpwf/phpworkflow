<?php
/**
 * Created by PhpStorm.
 * User: Rus
 * Description: PhpWorkflow TODO: add description
 */

namespace Workflow;
use Workflow\Storage\IStorage;

class Event {
    // Event properties
    const TYPE="type";
    const CONTEXT="context";
    const STATUS="status";

    // State of the event
    const EVENT_ACTIVE  = 0;
    const EVENT_HANDLED = 1;
    const EVENT_NO_CONSUMER = 2;

    /* @var int $workflow_id */
    protected $workflow_id=0;
    protected $event_id=0;
    protected $type;
    protected $status=0;
    /* @var \Workflow\Context $context */
    protected $context=null;

    public function __construct(array $data) {

        $properties=get_class_vars(__CLASS__);
        foreach($properties as $p => $v) {
            if(isset($data[$p])) {
                $this->$p=$data[$p];
            }
        }

        if($this->context instanceof Context) {
            return;
        }

        $context=new Context();

        if( is_array($this->context) ) {
            $context->set_all($this->context);
        }

        if( is_string($this->context)) {
            $context->unserialize($this->context);
        }

        $this->context=$context;
    }


    /**
     * @return int
     */
    public function get_id() {
        return $this->event_id;
    }


    /**
     * @return string
     */
    public function get_type() {
        return $this->type;
    }

    /**
     * @return int
     */
    public function get_status() {
        return $this->status;
    }

    /**
     * @param int $status of the event
     */
    public function set_status($status) {
        $this->status = $status;
    }


    public function send(IStorage $storage) {
        $storage->create_event($this);
    }

    public function set_key_data($key, $value) {
        $this->context->set($key, $value, Context::NAMESPACE_SUBSCRIPTION);
    }

    public function set_additional_data($key, $value) {
        $this->context->set($key, $value);
    }

    public function get_key_data() {
        return $this->context->get_all(Context::NAMESPACE_SUBSCRIPTION);
    }

    public function get_data($key) {
        $result=$this->context->get($key, Context::NAMESPACE_SUBSCRIPTION);
        if($result === null) {
            $result=$this->context->get($key);
        }
        return $result;
    }

    /** Returns the serialized context of the event
     * @return string
     */
    public function get_state() {
        return $this->context->serialize();
    }

    /**
     * Set the event state
     * @param string $serialized_state
     */
    public function set_state($serialized_state) {
        $this->context->unserialize($serialized_state);
    }

}
