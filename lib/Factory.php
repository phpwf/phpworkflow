<?php
/**
 * Created by PhpStorm.
 * User: Rus
 * Description: PhpWorkflow TODO: add description
 */

namespace Workflow;

class Factory implements IFactory {
    /**
     * @param string $type Type of the workflow
     * @return \Workflow\Workflow
     * @throws \Exception
     */
    public function new_workflow($type) {
        if(class_exists($type)) {
            return new $type();
        }
        throw new \Exception("Class $type not exists");
    }

    /**
     * @param string $type Type of the event
     * @return \Workflow\Event
     * @throws \Exception
     */
    public function new_event($type) {
        if(class_exists($type)) {
            return new $type();
        }
        throw new \Exception("Class $type not exists");
    }
}