<?php
/**
 * Created by PhpStorm.
 * User: Rus
 * Description: PhpWorkflow TODO: add description
 */


namespace Workflow\Engine;

use Workflow\ILogger;
use Workflow\Storage\IStorage;

abstract class AbstractEngine {
    /** @var AbstractEngine $instance */
    static protected $instance;
    /** @var IStorage $storage */
    protected $storage;
    /** @var ILogger $logger */
    protected $logger;

    protected function __construct(IStorage $storage, ILogger $logger) {
        $this->storage=$storage;
        $this->logger=$logger;
    }

    public static function get_instance(IStorage $storage, ILogger $logger) {
        if(empty(self::$instance)) {
            self::$instance=new static($storage, $logger);
        }
        return self::$instance;
    }

    abstract public function run();
}