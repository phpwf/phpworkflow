<?php
/**
 * Created by PhpStorm.
 * User: Rus
 * Description: PhpWorkflow TODO: add description
 */


namespace Workflow\Engine;

use Workflow\Workflow;
use Workflow\Event;

class Simple extends AbstractEngine {
    private $num_cycles=1;
    private $sleep_time=3;

    public function run() {
        while($this->num_cycles--) {
            $this->execute_workflows();
            sleep($this->sleep_time);
        }
    }

    private function execute_workflows() {
        $this->logger->debug("Start");
        $wf_ids=$this->storage->get_active_workflow_ids();

        foreach($wf_ids as $id) {
            /* @var \Workflow\Workflow $workflow */
            // Lock and get workflow object
            $workflow=$this->storage->get_workflow($id);
            if(empty($workflow)) {
                continue; // Workflow is locked
            }

            if(!$workflow->is_finished()) {
                // Function is executed after successful event processing
                $workflow->set_sync_callback(function(Workflow $workflow, Event $event) {
                    $this->storage->close_event($event);
                    $this->storage->save_workflow($workflow, false);
                });

                $events=$this->storage->get_events($id);
                $workflow->run($events);
            }
            // Save and unlock workflow
            $this->storage->save_workflow($workflow);
        }

        $this->storage->cleanup();
        $this->logger->debug("Finish");
    }

    public function set_params($num_cycles, $sleep_time) {
        $this->num_cycles=$num_cycles;
        $this->sleep_time=$sleep_time;
    }
}