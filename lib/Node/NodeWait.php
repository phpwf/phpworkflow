<?php
/**
 * Created by PhpStorm.
 * User: Rus
 * Description: PhpWorkflow TODO: add description
 */


namespace Workflow\Node;
use \Workflow\Workflow;

class NodeWait extends Base {
    const PRIORITY=1;
    const NODE_PREFIX="wait_";
    protected $timeout=0;

    public function __construct(array &$parameters) {
        parent::__construct($parameters);

        $this->timeout=$parameters[INode::P_TIMEOUT];
    }

    public function execute(Workflow $wf) {
        $wf->set_exec_time($wf->now() + $this->timeout);
        return $this->next_node_id;
    }

    public static function fix_node(Workflow $workflow, array &$node) {

        if(!isset($node[self::P_TIMEOUT])) {
            $node[self::P_TIMEOUT]=self::get_timeout_by_name($node[self::P_NAME]);
        }

        if($node[self::P_TIMEOUT] <= 0) {
            throw new \Exception("Wrong timeout value for node: " . print_r($node, true));
        }
    }

    private static function get_timeout_by_name($node_name) {
        // TODO implement this
        return 1;
    }

}