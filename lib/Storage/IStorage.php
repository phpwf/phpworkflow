<?php
/**
 * Created by PhpStorm.
 * User: Rus
 * Description: PhpWorkflow TODO: add description
 */

namespace Workflow\Storage;

use Workflow\IFactory;
use Workflow\ILogger;
use Workflow\Workflow;
use Workflow\Event;

interface IStorage {
    const WF_STATUS_FINISHED=0;
    const WF_STATUS_ACTIVE=1;
    const WF_STATUS_IN_PROGRESS=2;
    const WF_STATUS_FAILED=3;

    const EVENT_STATUS_ACTIVE=1;
    const EVENT_STATUS_PROCESSED=2;
    const EVENT_STATUS_NO_SUBSCRIBERS=3;

    /**
     * Returns the instance of Storage (IStorage interface)
     * @param $database_name
     * @param IFactory $factory
     * @param ILogger $logger
     * @return IStorage
     */
    public static function get_instance($database_name, IFactory $factory=null, ILogger $logger=null);

    /**
     * Creates new workflow in the storage
     * @param Workflow $workflow
     * @param bool $unique
     * @return boolean
     */
    public function create_workflow(Workflow $workflow,  $unique=false);

    /**
     * Creates new event in the storage
     * @param Event $event
     * @return boolean
     */
    public function create_event(Event $event);

    /**
     * Returns the list of workflows in status WF_STATUS_ACTIVE
     * @return array of int
     */
    public function get_active_workflow_ids();

    /**
     * Returns Workflow object. Lock this workflow in storage by default
     * @param int $id workflow id
     * @param boolean $lock
     * @return \Workflow\Workflow $workflow
     */
    public function get_workflow($id, $lock=true);

    /**
     * Save workflow object to storage. Unlock by default
     * @param Workflow $workflow
     * @return boolean
     */
    public function save_workflow(Workflow $workflow, $unlock=true);

    /**
     * Update event data after event was processed
     * @param Event $event
     * @return boolean
     */
    public function close_event(Event $event);


    /**
     * Restore workflows with errors during execution
     */
    public function cleanup();

    /**
     * @param $workflow_id
     * @return Event[] $events array of Event objects
     */
    public function get_events($workflow_id);

    /**
     * Store $log_message to log
     * @param $log_message
     * @param $workflow_id
     * @return void
     */
    public function write_log($log_message, $workflow_id=0);
}