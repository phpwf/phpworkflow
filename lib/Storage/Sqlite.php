<?php
/**
 * Created by PhpStorm.
 * User: Rus
 * Description: PhpWorkflow TODO: add description
 */

namespace Workflow\Storage;
use Workflow\Factory;
use Workflow\IFactory;
use Workflow\Logger;
use Workflow\ILogger;
use Workflow\Subscription;
use Workflow\Workflow;
use Workflow\Event;
use Workflow\SystemUtils;

class Sqlite implements IStorage{
    use SystemUtils;
    const DB_STRUCTURE='resources/sqlite_structure.sql';
    const CLEANUP_TIME=3600;

    /* @var IStorage $_storage */
    private static $_storage=null;

    private $db_structure;
    /* @var IFactory $factory */
    private $factory;
    /* @var ILogger $logger */
    private $logger;

    /* @var \SQLite3 $db */
    private $db;

    public static function get_instance($database_name, IFactory $factory=null, ILogger $logger=null) {
        if(empty(self::$_storage)) {
            self::$_storage=new Sqlite($database_name, $factory, $logger);
        }

        return self::$_storage;
    }

    protected function __construct($database_name, IFactory $factory=null, ILogger $logger=null) {

        $this->factory=empty($factory) ? new Factory() : $factory;
        $this->logger=empty($logger) ? new Logger(Logger::LOG_CONSOLE) : $logger;

        $this->db=new \SQLite3($database_name);
        $this->db_structure=WORKFLOW_PROJECT_ROOT_PATH.self::DB_STRUCTURE;

        if($this->is_created()) {
            return;
        }

        $this->create_storage();
    }

    private function create_subscription(Workflow $workflow, $is_new=true) {
        /**
         * @var Subscription $s
         */
        foreach($workflow->get_subscription($is_new) as $s) {
            // Workflow can be subscribe to several values of some event filter
            $values=is_array($s->context_value) ? $s->context_value : [$s->context_value];

            $s->context_key=\SQLite3::escapeString($s->context_key);
            $s->event_type=\SQLite3::escapeString($s->event_type);

            foreach($values as $v) {
                $v=\SQLite3::escapeString($v);

                $selected_id=$this->db->querySingle("SELECT `workflow_id` from `subscription`
                      WHERE `workflow_id` = {$workflow->get_id()} AND
                       `event_type` = '$s->event_type' AND
                       `context_key` = '$s->context_key' AND
                       `context_value` = '$v'
                      ");

                if($selected_id == $workflow->get_id()) {
                    continue;
                }

                $status=IStorage::EVENT_STATUS_ACTIVE;

                $sql="INSERT INTO `subscription` (`status`, `event_type`, `context_key`,
                `context_value`, `workflow_id`, `update_dt`)
                VALUES ($status, '$s->event_type', '$s->context_key',
                  '$v', {$workflow->get_id()}, datetime(0, 'unixepoch'))";

                if(!$this->db->exec($sql)) {
                    throw new \Exception("Can't insert data into subscription table: $sql");
                }
            }
        }
    }

    public function create_workflow(Workflow $workflow, $unique=false) {

        $type=\SQLite3::escapeString($workflow->get_type());
        $context=\SQLite3::escapeString($workflow->get_state());
        $start_time=$workflow->get_start_time();
        $status=IStorage::WF_STATUS_ACTIVE;

        $this->db->exec('BEGIN');
        try {

            $sql = "INSERT INTO `workflow` (`type`, `context`,`create_dt`,
                `start_dt`, `finish_dt`,  `status`)
                VALUES ('$type', '$context', datetime('now', 'localtime'),
                  datetime($start_time, 'unixepoch', 'localtime'), datetime(0, 'unixepoch'), $status
                )";

            if(!$this->db->exec($sql)) {
                throw new \Exception("Can't insert data into workflow table $sql");
            }
            $workflow_id=$this->db->lastInsertRowID();
            $workflow->set_id($workflow_id);

            $this->create_subscription($workflow);

            $this->db->exec('COMMIT');
        }
        catch(\Exception $e) {
            $this->logger->error($e->getMessage());
            $this->db->exec('ROLLBACK');
            return false;
        }

        return $workflow_id;
    }

    private function get_subscription($event_type, $filter_key='', $filter_value='') {
        $active=IStorage::EVENT_STATUS_ACTIVE;

        $event_type=\SQLite3::escapeString($event_type);
        $filter_key=\SQLite3::escapeString($filter_key);
        $filter_value=\SQLite3::escapeString($filter_value);

        $sql="SELECT `workflow_id` from `subscription`
                      WHERE
                       `status` = $active AND
                       `event_type` = '$event_type' AND
                       ( `context_key` = '$filter_key' AND `context_value` = '$filter_value'
                          OR `context_key` = '' AND `context_value` = ''
                       )
                      ";
        $results=$this->db->query($sql);

        $wf_ids=[];
        while ($row = $results->fetchArray()) {
           $wf_ids[]=$row['workflow_id'];
        }
        return $wf_ids;
    }

    private function get_subscriber_ids(Event $event) {
        $subscriber_ids=[];

        $key_data=$event->get_key_data();
        if(empty($key_data)) {
            return $this->get_subscription($event->get_type());
        }

        foreach ($key_data as $key => $value) {
            $s=$this->get_subscription($event->get_type(), $key, $value);
            $subscriber_ids=array_merge($subscriber_ids, $s);
        }

        return $subscriber_ids;

    }

    public function create_event(Event $event) {

        $type=\SQLite3::escapeString($event->get_type());
        $context=\SQLite3::escapeString($event->get_state());

        $status=IStorage::EVENT_STATUS_ACTIVE;
        $process_time=0;
        $workflow_ids=$this->get_subscriber_ids($event);

        // We will create processed event with workflow_id 0
        if(empty($workflow_ids)) {
            $workflow_ids[]=0;
            $status=IStorage::EVENT_STATUS_NO_SUBSCRIBERS;
            $process_time=time();
        }

        $this->db->exec('BEGIN');
        try {

            foreach($workflow_ids as $workflow_id) {
                $sql="INSERT INTO `event` (`type`, `context`,`create_dt`, `process_dt`,
                `status`, `workflow_id`)
                VALUES ('$type', '$context', datetime('now', 'localtime'), datetime($process_time, 'unixepoch', 'localtime'),
                  $status, $workflow_id
                )";

                if(!$this->db->exec($sql)) {
                    throw new \Exception("Can't insert data into event table $sql");
                }
            }

            $this->db->exec('COMMIT');
        }
        catch(\Exception $e) {
            $this->db->exec('ROLLBACK');
            return false;
        }

        return true;

    }

    /** Returns the array with IDs of workflows for execution
     * @return array
     */
    public function get_active_workflow_ids($type='') {

        $wf_active=IStorage::WF_STATUS_ACTIVE;
        $evt_active=IStorage::EVENT_STATUS_ACTIVE;
        $type_condition=empty($type) ? '' : " and `type` = '$type' ";

        // TODO optimize query
        $results = $this->db->query("SELECT workflow_id from `workflow`
            WHERE `status` = $wf_active
            and ( `start_dt` <= datetime('now', 'localtime') or
              workflow_id in (select workflow_id from `event` where `status` = $evt_active)
            )
            $type_condition order by `start_dt`");

        $wf_ids=[];
        while ($row = $results->fetchArray()) {
            $wf_ids[]=$row['workflow_id'];
        }

        return $wf_ids;
    }

    public function get_workflow($id, $lock=true) {

        $lock_id=$this->get_lock_string();

        if($lock) {
            $status=IStorage::WF_STATUS_IN_PROGRESS;
            $sql="UPDATE `workflow` SET `lock` = '$lock_id',
                `status` = $status,
                `error_count`=`error_count`+1
            WHERE `workflow_id` IN (SELECT `workflow_id` FROM `workflow` WHERE `workflow_id` = $id) AND `lock` = ''";

            $this->db->exec($sql);
        }

        $sql="SELECT * FROM `workflow` WHERE `workflow_id` = $id"
            .($lock ? " AND lock='$lock_id'" : "");

        $wf_data=$this->db->querySingle($sql,true);
        if(empty($wf_data)) {
            return false;
        }

        $workflow=$this->factory->new_workflow($wf_data['type']);
        $workflow->set_state($wf_data['context']);
        $workflow->set_id($id);

        // We finish workflow in case of error_limit reached
        if($workflow->many_errors($wf_data['error_count'])) {
            $workflow->finish();
        }

        return $workflow;
    }

    private function create_storage() {
        $structure=file_get_contents($this->db_structure);
        $this->db->exec($structure);
    }

    public function is_created() {
        $results = $this->db->query("select name from sqlite_master where type = 'table'");
        $tables=[];
        while ($row = $results->fetchArray()) {
            $tables[]=$row['name'];
        }

        // Check is the tables from description and database are the same
        $structure=file_get_contents($this->db_structure);
        if( !preg_match_all('/CREATE TABLE `(\w+)`/sim', $structure, $match)) {
            return false;
        }

        $intersection=array_intersect($tables,$match[1]);

        return count($intersection) == count($match[1]);
    }

    private function close_workflow($workflow_id) {
        $status=IStorage::WF_STATUS_FINISHED;
        $sql = "UPDATE `workflow` set `status`=$status
                WHERE `workflow_id` = $workflow_id
                )";

        if(!$this->db->exec($sql)) {
            throw new \Exception("Can't update data in workflow table $sql");
        }
        return true;
    }

    public function save_workflow(Workflow $workflow, $unlock=true) {
        if($workflow->is_finished()) {
            return $this->close_workflow($workflow->get_id());
        }

        $context=\SQLite3::escapeString($workflow->get_state());
        $start_time=$workflow->get_start_time();
        $workflow_id=$workflow->get_id();
        $status=IStorage::WF_STATUS_ACTIVE;

        $fields="`context`='$context',
                `start_dt`= datetime($start_time, 'unixepoch', 'localtime')
                "
            .($unlock ? ", `lock`='', `status`=$status, `error_count`=`error_count`-1" : "");

        $sql = "UPDATE `workflow` set $fields
                WHERE `workflow_id` = $workflow_id
                ";

        if(!$this->db->exec($sql)) {
            throw new \Exception("Can't update data in workflow table $sql");
        }

        return true;
    }

    public function close_event(Event $event) {
        $event_id=$event->get_id();
        $status=$event->get_status();

        $sql="UPDATE `event` set `status` = $status, `process_dt` = datetime('now', 'localtime')
              WHERE event_id = $event_id";

        if(!$this->db->exec($sql)) {
            throw new \Exception("Can't update data in event table $sql");
        }

        return true;
    }

    /**
     * Restore workflows with errors during execution
     * @return void
     */
    public function cleanup()
    {
        $status_in_progress=IStorage::WF_STATUS_IN_PROGRESS;
        $time_limit=$this->get_execution_time_limit();

        $results = $this->db->query("SELECT `workflow_id`, `lock` FROM `workflow`
            WHERE `status` = $status_in_progress
              AND `lock` <> ''
              AND 3600*24*(julianday('now','localtime') - julianday(`start_dt`)) > $time_limit
            ");

        while ($row = $results->fetchArray()) {
            list($host, $pid)=$this->get_host_pid_from_lock_string($row['lock']);
            if(self::process_exists($host,$pid)) {
                continue;
            }
            $status_active=IStorage::WF_STATUS_ACTIVE;

            $this->db->exec("UPDATE `workflow` set `lock` ='', `status` = $status_active
              WHERE `workflow_id` = {$row['workflow_id']}");
        }
    }

    private function get_execution_time_limit() {
        return self::CLEANUP_TIME;
    }

    /**
     * @param $workflow_id
     * @return Event[] $events array of Event objects
     */
    public function get_events($workflow_id)
    {
        // TODO: Implement get_events() method.
    }

    /**
     * Store $log_message to log
     * @param $log_message
     * @return void
     */
    public function write_log($log_message, $workflow_id=0)
    {
        $log_message=\SQLite3::escapeString($log_message);
        $sql="INSERT INTO `log` (`workflow_id`, `create_dt`, `log_text`)
                VALUES ($workflow_id, datetime('now', 'localtime'), '$log_message')";

        if(!$this->db->exec($sql)) {
            throw new \Exception("Can't insert data into log table: $sql");
        }
    }


}