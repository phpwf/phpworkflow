<?php
/**
 * Created by PhpStorm.
 * User: Rus
 * Description: PhpWorkflow TODO: add description
 */

namespace Workflow\Storage;
use Workflow\IFactory;
use Workflow\ILogger;
use Workflow\Workflow;
use Workflow\Event;

class Redis implements IStorage{

    /**
     * Creates new workflow in the storage
     * @param Workflow $workflow
     * @param bool $unique
     * @return boolean
     */
    public function create_workflow(Workflow $workflow, $unique = false)
    {
        // TODO: Implement create_workflow() method.
    }

    /**
     * Creates new event in the storage
     * @param Event $event
     * @return boolean
     */
    public function create_event(Event $event)
    {
        // TODO: Implement create_event() method.
    }

    /**
     * Save workflow object to storage. Unlock by default
     * @param Workflow $workflow
     * @return boolean
     */
    public function save_workflow(Workflow $workflow, $unlock = true)
    {
        // TODO: Implement save_workflow() method.
    }

    /**
     * Restore workflows with errors during execution
     * @return void
     */
    public function cleanup()
    {
        // TODO: Implement cleanup() method.
    }

    /**
     * Update event data after event was processed
     * @param Event $event
     * @return boolean
     */
    public function close_event(Event $event)
    {
        // TODO: Implement close_event() method.
    }

    /**
     * Returns the list of workflows in status WF_STATUS_ACTIVE
     * @return array of int
     */
    public function get_active_workflow_ids()
    {
        // TODO: Implement get_active_workflow_ids() method.
    }

    /**
     * Returns the instance of Storage (IStorage interface)
     * @param $database_name
     * @param IFactory $factory
     * @param ILogger $logger
     * @return IStorage
     */
    public static function get_instance($database_name, IFactory $factory = null, ILogger $logger = null)
    {
        // TODO: Implement get_instance() method.
    }

    /**
     * Returns Workflow object. Lock this workflow in storage by default
     * @param int $id workflow id
     * @param boolean $lock
     * @return \Workflow\Workflow $workflow
     */
    public function get_workflow($id, $lock = true)
    {
        // TODO: Implement get_workflow() method.
    }

    /**
     * @param $workflow_id
     * @return Event[] $events array of Event objects
     */
    public function get_events($workflow_id)
    {
        // TODO: Implement get_events() method.
    }


}