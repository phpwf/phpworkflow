<?php
/**
 * Created by PhpStorm.
 * User: Rus
 * Description: Responsible for processing data of workflow or event
 */

namespace Workflow;

class Context {
    const NAMESPACE_DEFAULT='default';
    const NAMESPACE_USER='user';
    const NAMESPACE_COUNTER='counter';
    const NAMESPACE_SUBSCRIPTION='subscription';

    private $data;

    public function __construct() {
        $this->data=[self::NAMESPACE_DEFAULT => []];
    }

    public function set($key, $value, $namespace=self::NAMESPACE_DEFAULT) {
        $this->data[$namespace][$key]=$value;
    }

    public function get($key, $namespace=self::NAMESPACE_DEFAULT) {
        if(isset($this->data[$namespace][$key])) {
            return $this->data[$namespace][$key];
        }
        return null;
    }

    public function set_all(array $data, $namespace=self::NAMESPACE_DEFAULT) {
        foreach($data as $k => $v) {
            $this->set($k, $v, $namespace);
        }
    }

    public function get_all($namespace=self::NAMESPACE_DEFAULT) {
        if(isset($this->data[$namespace])) {
            return $this->data[$namespace];
        }

        return [];
    }

    public function serialize() {
        $str=json_encode($this->data);
        return $str;
    }

    public function unserialize($str) {
        $buffer=json_decode($str, true);

        if(isset($buffer[self::NAMESPACE_DEFAULT])) {
            if(!is_array($buffer[self::NAMESPACE_DEFAULT])) {
                throw new \Exception("Wrong context format");
            }
        }
        else {
            $buffer[self::NAMESPACE_DEFAULT]=[];
        }

        $this->data=$buffer;
    }

}