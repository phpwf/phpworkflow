<?php
/**
 * Created by PhpStorm.
 * User: Rus
 * Description: Auto loader for PhpWorkflow classes
 */

if ( !defined('WORKFLOW_PROJECT_ROOT_PATH') ) {
    define('WORKFLOW_PROJECT_ROOT_PATH', dirname(__DIR__) . '/');
}

class Autoloader {
    static private $dir_list=[];
    static private $is_registered=false;

    public static function activate(array $lib_dir=[]) {

        array_push($lib_dir,WORKFLOW_PROJECT_ROOT_PATH.'lib/');

        $lib_dir=array_diff($lib_dir, self::$dir_list);

        if(empty($lib_dir)) {
            return;
        }
        self::$dir_list=array_merge(self::$dir_list, $lib_dir);

        if(self::$is_registered) {
            return;
        }

        self::$is_registered=true;

        spl_autoload_register(function($class_name) {

//                    error_log("TRY TO LOAD: $class_name");
            $class_name=str_replace('\\','/', $class_name);
            $parts=explode('/',$class_name);

            if($parts[0] == 'Workflow') {
                array_shift($parts);
            }

            $class_name=implode('/', $parts);
            $filename=$class_name . '.php';


            foreach(Autoloader::dir_list() as $d) {
                $full_name=$d.$filename;
//                error_log(" IN FILE: ".$full_name);
                if(file_exists($full_name)) {
//                    error_log(" LOADED: $class_name");
                    require_once($full_name);
                    return;
                }
            }
        });
    }

    public static function dir_list() {
        return self::$dir_list;
    }
}