<?php
/**
 * Created by PhpStorm.
 * User: Rus
 * Description: PhpWorkflow TODO: add description
 */

namespace Workflow;

use Workflow\Storage\IStorage;

class Logger implements ILogger{
    const TIMESTAMP_FORMAT='Y-m-d H:i:s';

    private $log_channel;
    private $external_logger=null;
    /** @var IStorage $storage */
    private $storage=null;

    public function __construct($log_channel=self::LOG_OFF, ILogger $external_logger=null) {
        $this->set_log_channel($log_channel);
        $this->external_logger=$external_logger;
    }

    public function set_storage(IStorage $storage) {
        $this->storage=$storage;
    }

    public function set_log_channel($log_channel) {
        $this->log_channel=$log_channel;
    }

    public function set_logger(ILogger $external_logger=null) {
        $this->external_logger=$external_logger;
    }

    public function debug($message) {
        $this->write_log($message, self::DEBUG);
    }

    public function info($message) {
        $this->write_log($message, self::INFO);
    }

    public function warn($message) {
        $this->write_log($message, self::WARN);
    }

    public function error($message) {
        $this->write_log($message, self::ERROR);
    }

    private function get_level_name($level='') {
        $arr=[self::DEBUG => 'debug', self::INFO => 'info', self::WARN => 'warn', self::ERROR => 'error'];
        if(isset($arr[$level])) {
            return $arr[$level];
        }

        return $arr[self::DEBUG];
    }

    private function write_log($message, $level=self::DEBUG) {

        if(!is_string($message)) {
            $message=var_export($message);
        }

        $level_name=$this->get_level_name($level);
        $timestamp=date(self::TIMESTAMP_FORMAT);

        switch($this->log_channel) {
            case self::LOG_STDOUT: {
                print_r("$timestamp $level_name: $message\n");
                break;
            }
            case self::LOG_CONSOLE: {
                error_log("$timestamp $level_name: $message");
                break;
            }
            case self::LOG_FILE: {
                // TODO implement log to file
                break;
            }
            case self::LOG_LOGGER: {
                if($this->external_logger == null) {
                    break;
                }
                $this->external_logger->$level_name($message);
                break;
            }
            case self::LOG_DATABASE: {
                $this->storage->write_log($message);
                break;
            }

        }
    }
}