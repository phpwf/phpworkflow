<?php
/**
 * Created by PhpStorm.
 * User: Rus
 * Description: PhpWorkflow example. Implementation of sale workflow.
 */

require_once(__DIR__.'/../../../lib/Autoloader.php');
Autoloader::activate();

use \Workflow\Storage\Sqlite;

class CreateSqliteStorage  {

    public function run($storage_file) {
        Sqlite::get_instance($storage_file);
    }
}

function usage() {
    die("Usage: php ".__FILE__." <new sqlite workflow db>");
}

if($argc != 2 ) {
    usage();
}

$fh=fopen($argv[1], 'w');
if($fh === false) {
    usage();
}
fclose($fh);

$engine=new CreateSqliteStorage();
$engine->run($argv[1]);