<?php
/**
 * Created by PhpStorm.
 * User: Rus
 * Description: PhpWorkflow example. Implementation of sale workflow.
 */

require_once(__DIR__.'/../lib/Autoloader.php');
Autoloader::activate([WORKFLOW_PROJECT_ROOT_PATH.'tests/Workflows/']);

use \Workflow\Event;
use \Workflow\Node\INode;

class WorkflowTest  {

    private $events_queue=[
        [Event::TYPE => TestSaleSample::EVENT_GOODS_SELECTED],
        [Event::TYPE => TestSaleSample::EVENT_SUPPLIER_SENT_GOODS],
        [Event::TYPE => TestSaleSample::EVENT_CUSTOMER_PAY_FOR_GOODS]
    ];

    public function run() {
        $wf=new TestSaleSample();

        while($wf->get_current_node_id() != INode::LAST_NODE) {
            $events=[];
            $e=$this->get_event_from_queue();
            if($e) {
                $events[]=$e;
            }
            $wf->run($events);
            sleep(1);
        }
    }

    public function get_event_from_queue() {
        $rand=rand(0,4);
        if($rand == 0 && count($this->events_queue) > 0) {
            $event=new Event(array_shift($this->events_queue));
            return $event;
        }
        return null;
    }
}

$engine=new WorkflowTest();
$engine->run();