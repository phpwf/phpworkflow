<?php
/**
 * Created by PhpStorm.
 * User: Rus
 * Description: PhpWorkflow TODO: add description
 */

require_once(__DIR__.'/../../lib/Autoloader.php');
Autoloader::activate();

use \Workflow\Engine\Simple;
use \Workflow\Storage\Sqlite;
use \Workflow\Logger;
use \Workflow\ILogger;

$db_name=WORKFLOW_PROJECT_ROOT_PATH.'tmp/workflow.sqlite';

$storage=Sqlite::get_instance($db_name);
$logger=new Logger(ILogger::LOG_DATABASE);
$logger->set_storage($storage);

$engine=Simple::get_instance($storage, $logger);
$engine->run();