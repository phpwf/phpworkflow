<?php
/**
 * Created by PhpStorm.
 * User: Rus
 * Description: PhpWorkflow TODO: add description
 */

require_once(__DIR__.'/../lib/Autoloader.php');
Autoloader::activate([WORKFLOW_PROJECT_ROOT_PATH.'tests/Workflows/',WORKFLOW_PROJECT_ROOT_PATH.'tests/']);

define('STATE_FILE_NAME', 'SaveLoadStateWF_state.json');

class SaveLoadStateWF extends TestSaleSample {
    private function load_state() {
        if(!file_exists(STATE_FILE_NAME)) {
            return;
        }

        $state=file_get_contents(STATE_FILE_NAME);

        $this->set_state($state);
    }

    private function save_state() {

        $state=$this->get_state();
        file_put_contents(STATE_FILE_NAME, $state );

    }

    public function run(array $events=[]) {
        $this->load_state();
        parent::run($events);
        $this->save_state();
    }
}

class SaveLoadStateTest extends SuccessFlowTest {

    public function setUp() {
        @unlink(STATE_FILE_NAME);
        $this->workflow=new SaveLoadStateWF();
    }

    public function tearDown() {
        unlink(STATE_FILE_NAME);
    }

}
