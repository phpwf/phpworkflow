<?php
/**
 * Created by PhpStorm.
 * User: Rus
 * Description: PhpWorkflow TODO: add description
 */

class TestDuplicateNodes extends \Workflow\Workflow {

    const CLASS_NAME = __CLASS__;
    const TEST_CONTEXT="test-context";

    function __construct() {
        $process_nodes = [
            ["action2"],
            ["action1"],
            ["wait_1", "timeout" => 1],
            ["action1"],
            ["wait_1", "timeout" => 1],
            ["action1"],
            ["goto_action2"],
        ];

        parent::__construct($process_nodes);
        $this->logger->set_log_channel(\Workflow\ILogger::LOG_STDOUT);
    }

// This methods should be implemented by programmer BEGIN

    public function action1() {
        $ctx=$this->get_context(self::TEST_CONTEXT);
        var_dump("Context: $ctx");
        $ctx++;
        $this->set_context(self::TEST_CONTEXT, $ctx);
    }

    public function action2() {
        $ctx=$this->get_context(self::TEST_CONTEXT);
        if(empty($ctx)) {
            return;
        }

        $ctx+=100;
        $this->set_context(self::TEST_CONTEXT, $ctx);

    }

    public function get_ctx() {
        return $this->get_context(self::TEST_CONTEXT);
    }

// This methods should be implemented by programmer BEGIN

    public function get_supported_business_objects() {
        return [];
    }
}
