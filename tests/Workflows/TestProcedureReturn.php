<?php
/**
 * Created by PhpStorm.
 * User: Rus
 * Description: PhpWorkflow TODO: add description
 */

class TestProcedureReturn extends \Workflow\Workflow {

    const CLASS_NAME = __CLASS__;
    const TEST_CONTEXT="test-context";

    function __construct() {
        $process_nodes = [
            ["action1"],
            ["call_procedure1"],
            ["action2"],
            ["call_procedure1"],
            ["goto_end"],
            ["proc_procedure1"],
            ["action3"],
            ["return"],
            ["end"]
        ];

        parent::__construct($process_nodes);
        $this->logger->set_log_channel(\Workflow\ILogger::LOG_STDOUT);
    }

// This methods should be implemented by programmer BEGIN

    public function action1() {
        $this->set_context(self::TEST_CONTEXT, __FUNCTION__);
        var_dump('Method: '.__METHOD__);
    }

    public function action2() {
        $this->set_context(self::TEST_CONTEXT, __FUNCTION__);
        var_dump('Method: '.__METHOD__);
    }

    public function action3() {
        $ctx=$this->get_context(self::TEST_CONTEXT);
        var_dump('A3: '.$ctx);
    }

// This methods should be implemented by programmer BEGIN

    public function get_supported_business_objects() {
        return [];
    }
}
