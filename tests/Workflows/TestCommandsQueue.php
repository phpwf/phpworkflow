<?php
/**
 * Created by PhpStorm.
 * User: Rus
 * Description: PhpWorkflow TODO: add description
 */

class TestCommandsQueue extends \Workflow\Workflow {

    const CLASS_NAME = __CLASS__;

    function __construct() {
        $process_nodes = [
            ["action1"],
            ["wait_1", "timeout" => 1],
            ["goto_action3"],
            ["action2"],
            ["goto_end"],
            ["action3"],
            ["wait_2", "timeout" => 1],
            ["goto_action2"],
            ["end"]
        ];

        parent::__construct($process_nodes);
        $this->logger->set_log_channel(\Workflow\ILogger::LOG_STDOUT);
    }

// This methods should be implemented by programmer BEGIN

    public function end() {
        var_dump("WF Finished");
    }

    public function action1() {
        var_dump('Method: '.__METHOD__);
    }

    public function action2() {
        var_dump('Method: '.__METHOD__);
    }

    public function action3() {
        var_dump('Method: '.__METHOD__);
    }

// This methods should be implemented by programmer BEGIN
    public function get_supported_business_objects() {
        return [];
    }
}
