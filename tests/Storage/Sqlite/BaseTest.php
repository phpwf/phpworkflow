<?php
/**
 * Created by PhpStorm.
 * User: Rus
 * Description: PhpWorkflow TODO: add description
 */

require_once(__DIR__.'/../../../lib/Autoloader.php');
Autoloader::activate([WORKFLOW_PROJECT_ROOT_PATH.'tests/Workflows/',WORKFLOW_PROJECT_ROOT_PATH.'tests/']);

use Workflow\Storage\Sqlite;

class SqliteBaseTest extends PHPUnit_Framework_TestCase {
    const CUSTOMER_ID=777;
    const OTHER_CUSTOMER_ID=333;
    static private $db_name;
    /* @var \Workflow\Storage\Sqlite $storage */
    static private $storage;

    /**
     * @var SQLite3 $sqlite3
     */
    static private $sqlite3;

    public static function setUpBeforeClass() {
        self::$db_name=WORKFLOW_PROJECT_ROOT_PATH.'tmp/workflow_test.sqlite';
        if(file_exists(self::$db_name)) {
           unlink(self::$db_name);
        }

        self::$storage=Sqlite::get_instance(self::$db_name);
        self::$sqlite3=new SQLite3(self::$db_name);
    }

    public function test_all() {
        $this->t1_init();
        $this->t2_lock_unlock_cleanup();
    }

    public function t1_init() {

        $wf=new TestSaleSample();
        $wf->set_context(TestSaleSample::WF_KEY_CUSTOMER, self::CUSTOMER_ID);
        $workflow_id=self::$storage->create_workflow($wf);

        $wf_data=self::$sqlite3->querySingle("select * from `workflow`",true);

        $this->assertNotEmpty($wf_data);

        $this->assertEquals($wf_data['type'], TestSaleSample::class);
        $this->assertEquals($wf_data['status'], \Workflow\Storage\IStorage::WF_STATUS_ACTIVE);

        $subscription_cnt=self::$sqlite3->querySingle("select count(*) from `subscription`");
        $this->assertEquals($subscription_cnt, count($wf->get_subscription(true)));

        $event=new \Workflow\Event(['type' => TestSaleSample::EVENT_GOODS_SELECTED]);
        $event->set_key_data(TestSaleSample::WF_KEY_CUSTOMER, self::CUSTOMER_ID);

        self::$storage->create_event($event);
        $evt_data=self::$sqlite3->querySingle("select * from `event` where `workflow_id` = $workflow_id",true);
        $this->assertNotEmpty($evt_data);
        $this->assertEquals($evt_data['status'], \Workflow\Storage\IStorage::EVENT_STATUS_ACTIVE);

        $event=new \Workflow\Event(['type' => TestSaleSample::EVENT_GOODS_SELECTED]);
        self::$storage->create_event($event);
        $evt_data=self::$sqlite3->querySingle("select * from `event` where `workflow_id` <> $workflow_id",true);
        $this->assertNotEmpty($evt_data);
        $this->assertEquals($evt_data['status'], \Workflow\Storage\IStorage::EVENT_STATUS_NO_SUBSCRIBERS);

        $type=TestSaleSample::EVENT_CUSTOMER_PAY_FOR_GOODS;
        $event=new \Workflow\Event(['type' => $type]);
        self::$storage->create_event($event);
        $evt_data=self::$sqlite3->querySingle("select * from `event`
          where `workflow_id` = $workflow_id and `type` = '$type'",true);
        $this->assertNotEmpty($evt_data);
        $this->assertEquals($evt_data['status'], \Workflow\Storage\IStorage::EVENT_STATUS_ACTIVE);

    }

    private function check_locked($workflow_id, $is_my=true) {
        $wf_arr_locked=self::$sqlite3->querySingle("select * from workflow where workflow_id = $workflow_id", true);

        if($is_my) {
            list($host, $pid)=self::$storage->get_host_pid_from_lock_string($wf_arr_locked['lock']);
            $this->assertEquals($host, gethostname());
            $this->assertEquals($pid, getmypid());
        }
        else {
            $this->assertNotEmpty($wf_arr_locked['lock']);
        }
        $this->assertEquals($wf_arr_locked['status'], \Workflow\Storage\IStorage::EVENT_STATUS_PROCESSED);
        $this->assertEquals($wf_arr_locked['error_count'], 1);
    }

    private function check_unlocked($workflow_id) {
        $wf_arr_unlocked=self::$sqlite3->querySingle("select * from workflow where workflow_id = $workflow_id", true);

        $this->assertEmpty($wf_arr_unlocked['lock']);
        $this->assertEquals($wf_arr_unlocked['status'], \Workflow\Storage\IStorage::EVENT_STATUS_ACTIVE);
//        $this->assertEquals($wf_arr_unlocked['error_count'], 0);
    }

    /**
     * @depends test_init
     */
    public function t2_lock_unlock_cleanup() {

        $wf_arr=self::$sqlite3->querySingle("select * from workflow where rowid = 1", true);

        $workflow_id=$wf_arr['workflow_id'];
        $workflow=self::$storage->get_workflow($workflow_id); // Lock
        $this->assertInstanceOf(\Workflow\Workflow::class, $workflow);

        $this->check_locked($workflow_id);

        $workflow->set_exec_time(time());

        self::$storage->save_workflow($workflow);

        $workflow=self::$storage->get_workflow($workflow_id); // Lock
        $this->check_locked($workflow_id);
        self::$storage->cleanup();
        $this->check_locked($workflow_id);

        $lock=self::$storage->get_lock_string(gethostname(),1234567);
        self::$sqlite3->exec("UPDATE `workflow` set `lock` = '$lock' WHERE workflow_id = $workflow_id");

        self::$storage->cleanup();
        $this->check_locked($workflow_id, $is_my=false);

        self::$sqlite3->exec("UPDATE `workflow` set `start_dt` = datetime('1980-01-01', 'localtime') WHERE workflow_id = $workflow_id");

        self::$storage->cleanup();
        $this->check_unlocked($workflow_id);


    }

}
