<?php
/**
 * Created by PhpStorm.
 * User: Rus
 * Description: PhpWorkflow TODO: add description
 */

require_once(__DIR__ . '/../../lib/Autoloader.php');
Autoloader::activate([WORKFLOW_PROJECT_ROOT_PATH.'tests/Workflows/',WORKFLOW_PROJECT_ROOT_PATH.'tests/']);

use \Workflow\Node\INode;

class SimpleTest extends PHPUnit_Framework_TestCase {

    function test_simple() {
        $wf=new TestCommandsQueue();
        $wf->run();
        $this->assertEquals('goto_action3', $wf->get_current_node_name());
        $wf->set_exec_time(0);
        $wf->run();
        $this->assertEquals('goto_action2', $wf->get_current_node_name());
        $wf->set_exec_time(0);
        $wf->run();
        $this->assertEquals(INode::LAST_NODE, $wf->get_current_node_name());
        $wf->run();
        $this->assertEquals(INode::LAST_NODE, $wf->get_current_node_name());
    }

}
