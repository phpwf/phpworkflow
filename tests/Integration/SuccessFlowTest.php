<?php
/**
 * Created by PhpStorm.
 * User: Rus
 * Description: PhpWorkflow TODO: add description
 */

require_once(__DIR__ . '/../../lib/Autoloader.php');
Autoloader::activate([WORKFLOW_PROJECT_ROOT_PATH.'tests/Workflows/',WORKFLOW_PROJECT_ROOT_PATH.'tests/']);

use \Workflow\Event;
use \Workflow\Node\INode;

class SuccessFlowTest extends PHPUnit_Framework_TestCase {
    protected $workflow;

    public function setUp() {
        $this->workflow=new TestSaleSample();
    }

    function test_success() {
        /** @var \Workflow\Workflow $wf */
        $wf=$this->workflow;
        $wf->run();
        $this->assertEquals('goto_select_goods', $wf->get_current_node_name());

        $event=new Event([Event::TYPE => TestSaleSample::EVENT_GOODS_SELECTED]);

        $wf->run([$event]);
        $node_name=$wf->get_current_node_name();
        $this->assertContains($node_name, ['goto_if_customer_pay_for_goods', 'report_no_goods']);

        if($wf->get_current_node_name() === 'report_no_goods') {
            $event=new Event([Event::TYPE => TestSaleSample::EVENT_SUPPLIER_SENT_GOODS]);
            $wf->run([$event]);
        }

        $this->assertEquals('goto_if_customer_pay_for_goods', $wf->get_current_node_name());
        $wf->set_exec_time(0);
        $wf->run();
        $this->assertEquals('goto_if_customer_pay_for_goods', $wf->get_current_node_name());

        $event=new Event([Event::TYPE => TestSaleSample::EVENT_CUSTOMER_PAY_FOR_GOODS]);
        $wf->run([$event]);

        $this->assertEquals(INode::LAST_NODE, $wf->get_current_node_name());
    }

}
