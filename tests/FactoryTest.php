<?php
/**
 * Created by PhpStorm.
 * User: Rus
 * Description: PhpWorkflow TODO: add description
 */
use \Workflow\Factory;
require_once(__DIR__.'/../lib/Autoloader.php');
Autoloader::activate([WORKFLOW_PROJECT_ROOT_PATH.'tests/Workflows/',WORKFLOW_PROJECT_ROOT_PATH.'tests/']);

class FactoryTest extends PHPUnit_Framework_TestCase {

    function test_factory() {

        $wf=(new Factory())->new_workflow(TestSaleSample::class);
        $this->assertInstanceOf(TestSaleSample::class, $wf);

        $this->setExpectedException(
            'Exception', 'Class NonExistentClass not exists'
        );

        $wf=(new Factory())->new_workflow('NonExistentClass');
    }

}
