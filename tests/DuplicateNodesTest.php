<?php
/**
 * Created by PhpStorm.
 * User: Rus
 * Description: PhpWorkflow TODO: add description
 */

require_once(__DIR__.'/../lib/Autoloader.php');
Autoloader::activate([WORKFLOW_PROJECT_ROOT_PATH.'tests/Workflows/',WORKFLOW_PROJECT_ROOT_PATH.'tests/']);

class DuplicateNodesTest extends PHPUnit_Framework_TestCase {

    function test_duplicate() {
        $wf=new TestDuplicateNodes();
        $wf->run();
        $this->assertEquals("action1", $wf->get_current_node_name());
        $this->assertEquals(4, $wf->get_current_node_id());
        sleep(2);
        $wf->run();
        $this->assertEquals("action1", $wf->get_current_node_name());
        $this->assertEquals(6, $wf->get_current_node_id());
        sleep(2);
        $wf->run();
        $this->assertEquals("action1", $wf->get_current_node_name());
        $wf->run();
        $this->assertEquals("action1", $wf->get_current_node_name());
        $text_value=$wf->get_ctx();

        $this->assertEquals($text_value, 104);
    }

}
